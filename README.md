# Crystal Image Downloader

[![Language](https://img.shields.io/badge/language-crystal-776791.svg)](https://github.com/crystal-lang/crystal)
![Status](https://img.shields.io/badge/status-WIP-blue.svg)

A simple image scraper that downloads all images in `<img>` tags from a site.
Can be extended to work with different cli flags

## Installation/Usage

- clone project
- Run `shards` to install dependancies needed
- `crystal build src/image-downloader.cr`
- `mkdir bin && mv image-downloader bin/ && mv image-downloader.dwarf bin/`
- Run with `./bin/image-downloader`

## Development

Make changes to `src/image-downloader.cr`, just make sure to adhere to the current structure and use the contribution guide below

## Contributing

1. Fork it (<https://github.com/your-github-user/image-downloader/fork>)
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request

## Contributors

- [MaterialFuture](https://github.com/materialfuture) - creator and maintainer
